#ifndef FORMTABLE_H
#define FORMTABLE_H

#include <QWidget>
#include <QDir>
#include "ui_formtable.h"

class FormTable : public QWidget, public Ui_FormTable
{
    Q_OBJECT
public:
    FormTable(QWidget *parent=0);

private slots:
    void save(void);
    void print(void);

private:
    QDir savedir;
};

#endif // FORMTABLE_H
