#Resist-Qt

This program helps you choose a pair of resistors that most closely match a given needed ratio based on the analog design.  If this seems trivial, you are mistaken - try finding two resistors using standard values that are closest to, say, 7.45.  You would have to try a lot of combinations to get close, and even then finding the very closest pair might take a long time.

##Building

Qt5 is required.

Use qmake and then make.  If it builds, then an executable is created that you can run.

##Usage

It's a GUI program, so just click around to see what it does.
