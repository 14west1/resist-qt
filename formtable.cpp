#include <QFileDialog>
#include <QMessageBox>
#include <QPrinter>
#include <QPrintDialog>
#include "formtable.h"

FormTable::FormTable(QWidget *parent) : QWidget(parent, Qt::Window)
{
    setupUi(this);

    connect(pbSave, SIGNAL(clicked()), this, SLOT(save()));
    connect(pbPrint, SIGNAL(clicked()), this, SLOT(print()));
}

void FormTable::save(void)
{
    // Save to file
    if (edResults->toPlainText().isEmpty()) return;
    QString dir = savedir.absolutePath();
    QString name = QFileDialog::getSaveFileName(this,
                                                "Save results", dir, "All Files (*)");
    if (name.isEmpty()) return;
    QFile file(name);
    if (!file.open(QIODevice::WriteOnly))
    {
        QString s("Cannot write file ");
        s += name;
        QMessageBox::critical(this,"File read error", s);
        return;
    }

    file.write(edResults->toPlainText().toLatin1());
    file.write("\n");

    file.close();
    savedir = name;
}

void FormTable::print(void)
{
    // Print results
    if (edResults->toPlainText().isEmpty()) return;
    QPrinter printer;

    QPrintDialog *dialog = new QPrintDialog(&printer, this);
    dialog->setWindowTitle(tr("Print Document"));
    if (edResults->textCursor().hasSelection())
        dialog->setOption(QAbstractPrintDialog::PrintSelection, true);
    dialog->setOption(QAbstractPrintDialog::PrintToFile, true);
    if (dialog->exec() != QDialog::Accepted)
        return;

    edResults->print(&printer);
}

