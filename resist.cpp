#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>
#include <QPrinter>
#include <QPrintDialog>
#include <stdio.h>
#include "resist.h"
#include "formtable.h"
#include "math.h"

int seriesValues[] = {fivePct, onePct, pointOnePct};
int table5pct[] = {10,11,12,13,15,16,18,20,22,24,27,30,33,36,39,43,47,51,56,62,68,75,82,91};

Resist::Resist(QWidget *parent, Qt::WindowFlags flags) : QMainWindow(parent, flags)
{
    setupUi(this);

    createTables();

    connect(pbGo, SIGNAL(clicked(void)), this, SLOT(goButton(void)));
    connect(actionTable5, SIGNAL(triggered()), this, SLOT(show5table()));
    connect(actionTable1, SIGNAL(triggered()), this, SLOT(show1table()));
    connect(actionTable01, SIGNAL(triggered()), this, SLOT(show01table()));
    connect(pbSave, SIGNAL(clicked()), this, SLOT(save()));
    connect(pbPrint, SIGNAL(clicked()), this, SLOT(print()));
    connect(pbClear, SIGNAL(clicked()), this, SLOT(clear()));
    connect(actionAbout_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(action_About, SIGNAL(triggered()), this, SLOT(about()));
    setTableLabels(ctRatio);
}

void Resist::setTableLabels(CalcType ct)
{
    QStringList list;
    list << "R1" << "R2" << "R1/R2" << "Desired R1/R2";
    if (ct == ctInverting)
        list << "Gain Error (%)";
    else
        list << "Error (%)";

    if (ct == ctNonInvert)
    {
        list << "Gain error (%)";
        tblResults->setColumnCount(6);
    }
    else
        tblResults->setColumnCount(5);
    tblResults->setHorizontalHeaderLabels(list);
    tblResults->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

    // Set Header text
    outputHdr.clear();
    QString s("Calculation type: ");
    QString r;
    switch (ct)
    {
    case ctRatio:
        s += "Plain ratio";
        r = "Ratio = ";
        break;
    case ctDivider:
        s += "Resistor divider";
        r = "Ratio = ";
        break;
    case ctInverting:
        s += "Inverting amplifier";
        r = "Gain = ";
        break;
    case ctNonInvert:
        s += "Non-inverting amplifier";
        r = "Gain = ";
        break;
    default:
        return;
    }
    outputHdr.append(s);
    r += "%1";
    outputHdr.append(r);
    outputHdr.append("");
    s = "R1        R2        R1/R2     Target R  Error(%)";
    if (ct == ctNonInvert)
        s += "Gain Error(%)";
    outputHdr.append(s);
}

void Resist::goButton(void)
{
    double input;
    input = edInput->text().toDouble();

    CalcType ct = (CalcType) tabWidget->currentIndex();
    calculate(input, ct);
}

void Resist::clear(void)
{
    // Clear the table
    tblResults->clearContents();
}

void Resist::calculate(double input, CalcType ct)
{
    // Calculate resistors, print them
    int pctNdx = cbTolerance->currentIndex();

    setTableLabels(ct);
    // Fill in header label with input ratio
    QString s = outputHdr.at(1);
    s = s.arg(input, 0, 'f', 4);
    outputHdr.removeAt(1);
    outputHdr.insert(1, s);

    // Calculate ratio from input type.
    double ratio;
    if (input == 0) return;
    ratio = input;
    switch (ct)
    {
    case ctRatio:
        break;
    case ctDivider:
        if (ratio <= 0 || ratio >= 1)
        {
            QMessageBox::critical(NULL, "Invalid Input", "Input ratio must be between zero and one.");
            return;
        }
        ratio = (1/ratio) - 1;
        break;
    case ctInverting:
        ratio = fabs(ratio);
        break;
    case ctNonInvert:
        ratio = ratio - 1;
        break;
    }

    QStringList strings[10];
    find_matches(ratio, pctNdx, strings);
    // Put results into edit box
    for (int i=0;i<10;i++)
    {
        QStringList sl = strings[i];
        for (int j=0;j<sl.count();j++)
        {
            QString s = sl.at(j);
            QTableWidgetItem *ti = new QTableWidgetItem(s);
            tblResults->setItem(i, j, ti);
        }
        // Gain error
        if (ct == ctNonInvert)
        {
            // gain = R1/R2 + 1, ideal gain = input.  Error is ((R1/R2+1)/input - 1)*100
            double R1, R2;
            R1 = sl.at(0).toDouble();
            R2 = sl.at(1).toDouble();
            double err = R1/R2;
            err += 1;
            err /= input;
            err -= 1;
            err *= 100;
            QString s = QString("%1").arg(err, 0, 'f', 4);
            QTableWidgetItem *ti = new QTableWidgetItem(s);
            tblResults->setItem(i,5,ti);
        }
    }
    tblResults->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);
}

int Resist::compareMatch(const void *a, const void *b)
{
    Match *m1 = (Match *)a;
    Match *m2 = (Match *)b;
    if (fabs(m1->error) < fabs(m2->error))
        return -1;
    if (fabs(m1->error) > fabs(m2->error))
        return 1;
    return 0;
}

void Resist::find_matches(double ratio, int pctNdx, QStringList *strings)
{
   // searches for pairs of resistor values whose ratio is closest to input ratio
   int x;
   int y,z;
   Match *matchPtr;
   double trial_ratio;
   double mult;
   char str[500];
   struct Match match[10];


   /* init the topten structure.                                        */
   for(x=0;x<10;x++)
   {
       match[x].error = 20;    /* so that null entries don't print*/
       match[x].r1ndx = 0;     /* so that null entries don't print*/
       match[x].r2ndx = 0;     /* so that null entries don't print*/
   }

   ValList table = tables[pctNdx];

   /* scan the table - each value compared to each value.               */
   int values = table.count();
   for(x=0;x<values;x++)
   {
       for(y=0;y<values;y++)
       {
           mult = 1;
           /* divide R1 by R2, check ratio.                   */
           trial_ratio = (double)table[x]/(double)table[y];

           /* multiply by 10 until we're close.                       */
           while (trial_ratio*5 < ratio)
           {
               trial_ratio *= 10;
               mult *= (double)10;
           }

           /* divide by 10 until we're close.                 */
           while (trial_ratio > ratio*5)
           {
               trial_ratio /= 10;
               mult /= 10;
           }

           /* scan match structure for highest ratio error.    */
           double highestErr=0;
           for (z=0;z<10;z++)
           {
               if (fabs(match[z].error) > highestErr)
               {
                   highestErr = fabs(match[z].error);
                   matchPtr = &match[z];
               }
           }
           // Replace highest error with new match
           if (highestErr > 0)
           {
               double error = (trial_ratio - ratio)/ratio;
               if (fabs(error) < fabs(matchPtr->error))
               {
                   matchPtr->error = error;
                   matchPtr->r1ndx = x;     /* index to R1 value    */
                   matchPtr->r2ndx = y;
                   matchPtr->multiplier = mult;
               }
           }
       }
   }

   // Sort the list
   qsort(match, 10, sizeof(Match), compareMatch);

   /* printout top ten.                                         */
   for (x=0;x<10;x++)
     {matchPtr = &match[x];
        if (matchPtr->error < 20)
          {
             mult = matchPtr->multiplier;
             sprintf(str, "%.0f", 
                     (float)table[matchPtr->r1ndx] * ((mult > 1) ? (double)mult : (double)1));
             strings[x].append(str);
             sprintf(str, "%.0f",
                     (float)table[matchPtr->r2ndx] / ((mult < 1) ? (double)mult : (double)1));
             strings[x].append(str);
             sprintf(str, "%.5f",
                     (float)table[matchPtr->r1ndx]/(float)table[matchPtr->r2ndx] * mult);
             strings[x].append(str);
             sprintf(str, "%.5f", ratio);
             strings[x].append(str);
             sprintf(str, "%.4f", (matchPtr->error)*100);
             strings[x].append(str);
          }
     }
}


void Resist::showTable(int pctNdx)
{

    FormTable *ft= new FormTable(this);
    QString title;
    switch (pctNdx)
    {
    case 0:
        title = "5% Resistor Table";
        break;
    case 1:
        title = "1% Resistor Table";
        break;
    case 2:
        title = "0.1% Resistor Table";
        break;
    default:
        break;
    }
    ft->setWindowTitle(title);
    ft->edResults->appendPlainText(title + "\n");

    ValList table = tables[pctNdx];
    int values = table.count();         // number of resistor values in table
    int row,col;
    int columns=6;
    int rows;
    rows = values/columns + 1;

    for (row=0;row<rows;row++)
    {
        QString s;
        for (col=0;col<columns;col++)
        {
            int index=col*rows + row;
            if (index < values)
            {
                QString n = QString("%1").arg(table[index]);
                QString spaces = QString().fill(' ',10-n.length());
                n += spaces;
                s += n;
            }
        }
        ft->edResults->appendPlainText(s);
    }
    ft->show();
}

void Resist::createTables()
{
    // generates tables of 5%, 1%, and 0.1% standard resistor values
    tables.resize(3);
    for (int i=0;i<3;i++)
    {
        ValList vlist;
        tables[i] = vlist;
    }
    for (int pct=0;pct<3;pct++)
    {
        int valueCount;
        switch (pct)
        {
        case 0:
            valueCount = fivePct;
            break;
        case 1:
            valueCount = onePct;
            break;
        case 2:
            valueCount = pointOnePct;
            break;
        default:
            return;
        }

        tables[pct].resize(valueCount);
        if (valueCount == fivePct)
        {
            // Copy 5 percent table over
            for (int i=0;i<valueCount;i++)
                tables[pct][i] = table5pct[i];
        }
        else
        {
            for (int i=0;i<valueCount;i++)
            {
                double r = pow((double)10,(double)i/valueCount) * 100;
                tables[pct][i] = (int)(r + .5);
                // fudge factor here, for some reason the 0.1% table deviates from the math
                if (tables[pct][i] == 919)
                    tables[pct][i] = 920;
            }
        }
    }
}

void Resist::save(void)
{
    // Save to file
    if (tblResults->item(1,1)->text().isEmpty()) return;
    QString dir = savedir.absolutePath();
    QString name = QFileDialog::getSaveFileName(this,
                                                "Save results", dir, "All Files (*)");
    if (name.isEmpty()) return;
    QFile file(name);
    if (!file.open(QIODevice::WriteOnly))
    {
        QString s("Cannot write file ");
        s += name;
        QMessageBox::critical(this,"File read error", s);
        return;
    }

    // Write to file
    for (int i=0;i<outputHdr.count();i++)
    {
        // Write header lines
        file.write(outputHdr.at(i).toLatin1(), (qint64)(outputHdr.at(i).length()));
        file.write("\n");
    }
    for (int row=0;row<tblResults->rowCount();row++)
    {
        QString s;
        for (int col=0;col<tblResults->columnCount();col++)
        {
            s += tblResults->item(row, col)->text();
            s += "          ";
            s.resize((col+1)*10);
        }
        QByteArray b(s.toLatin1(), s.length());
        file.write(b);
        file.write("\n");
    }

    file.close();
    savedir = name;

}

void Resist::print(void)
{
    // Print results
    if (tblResults->item(1,1)->text().isEmpty()) return;
    QPrinter printer;

    QPrintDialog *dialog = new QPrintDialog(&printer, this);
    dialog->setWindowTitle(tr("Print Document"));
    dialog->setOption(QAbstractPrintDialog::PrintToFile, true);
    if (dialog->exec() != QDialog::Accepted)
        return;

    // Print to printer
    QPlainTextEdit ed;
    for (int i=0;i<outputHdr.count();i++)
    {
        // Write header lines
        ed.appendPlainText(outputHdr.at(i));
    }
    for (int row=0;row<tblResults->rowCount();row++)
    {
        QString s;
        for (int col=0;col<tblResults->columnCount();col++)
        {
            s += tblResults->item(row, col)->text();
            s += "          ";
            s.resize((col+1)*10);
        }
        ed.appendPlainText(s);
    }
    QFont f = ed.font();
    f.setFamily("Courier New");
    ed.setFont(f);
    ed.print(&printer);
}

void Resist::about()
{
    QMessageBox::about(this, "About Resistor Calculator",
                       "Resistor Calculator\n"
                       "Copyright © 2018 Blake Leverett\n"
                       "GPL V3");
}
