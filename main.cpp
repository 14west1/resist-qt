#include <QApplication>
#include <QSettings>

#include "resist.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv, true);
    
    Resist w;
    w.show();        
    return app.exec();
}
