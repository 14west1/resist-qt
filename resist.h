#ifndef RESIST_H
#define RESIST_H

#include <QMainWindow>
#include <QDir>
#include <QVector>
#include "ui_mainwindow.h"

typedef enum {ctRatio, ctDivider, ctInverting, ctNonInvert } CalcType;
enum {fivePct=24, onePct=96, pointOnePct=192};
typedef QVector <int> ValList;

struct Match {
    int r1ndx;        /* R1's position in table.                      */
    int r2ndx;        /* R2's position in table.                      */
    double multiplier;/* multiplier for resistors.                  */
    double error;     /* R1/R2 - calc_ratio                           */
};

class Resist : public QMainWindow , public Ui_MainWindow
{
    Q_OBJECT
public:
    Resist(QWidget *parent=0, Qt::WindowFlags flags=0);
public slots:
    void goButton(void);
    void calculate(double input, CalcType ct);
    void show5table(void)       { showTable(0); }
    void show1table(void)       { showTable(1); }
    void show01table(void)      { showTable(2); }
    void save(void);
    void print(void);
    void clear(void);
    void about();

private:
    void createTables();
    void find_matches(double ratio, int pctNdx, QStringList *strings);
    void showTable(int pctNdx);
    QVector <ValList> tables;
    QDir savedir;
    static int compareMatch(const void *a, const void *b);
    void setTableLabels(CalcType ct);
    QStringList outputHdr;
};
#endif // RESIST_H
